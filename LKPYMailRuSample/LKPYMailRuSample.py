import unittest
from selenium import webdriver
from PageObjects.Login import LoginPage
from PageObjects.Main import Main
 
from selenium.webdriver.common.by import By

class TestPages(unittest.TestCase):
    def setUp(self):
        self.driver = webdriver.Chrome('D:\\DEV\\Lib\\chromedriver.exe')
        #self.driver = webdriver.Firefox()
        #self.driver = webdriver.PhantomJS()
        #self.driver = webdriver.Ie()

    def test_send_mail(self):
        mail_page = Main(self.driver)
        login_page = LoginPage(self.driver)
        login_page.navigate('https://mail.ru')
        login_page.login_user('testuser', 'testpassword')

        mail_page.show_create_new_mail_page()
        mail_page.fill_and_send_mail('some@email.ru', 'Some test subject', 'some test text in body mail')

    def tearDown(self):
        mail_page = Main(self.driver)
        if mail_page.is_logged_in:
            mail_page.navigate("https://mail.ru")
            mail_page.logout_user()
        self.driver.close()

if __name__ == "__main__":
    suite = unittest.TestLoader().loadTestsFromTestCase(TestPages)
    unittest.TextTestRunner(verbosity=2).run(suite)