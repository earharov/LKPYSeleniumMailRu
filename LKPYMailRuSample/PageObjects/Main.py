from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC

class Main(object):
    
    def __init__(self, driver):
        self.driver = driver

    def navigate(self, url):
        self.driver.get(url)

    def show_create_new_mail_page(self):
        self.driver.find_element_by_css_selector('span.b-toolbar__btn__text.b-toolbar__btn__text_pad').click() 

    def fill_and_send_mail(self, to, subject, text):
        print 'test'
        self.driver.find_element_by_css_selector('textarea.js-input.compose__labels__input').click() 
        self.driver.find_element_by_css_selector('textarea.js-input.compose__labels__input').clear() 
        self.driver.find_element_by_css_selector('textarea.js-input.compose__labels__input').send_keys(to)
        
        self.driver.find_element_by_name('Subject').clear()
        self.driver.find_element_by_name('Subject').send_keys(subject)
        iframe = self.driver.find_element_by_xpath("//iframe[starts-with(@id,'toolkit-')]")
        self.driver.switch_to_frame(iframe)
        self.driver.find_element_by_id('tinymce').click()
        self.driver.find_element_by_id('tinymce').send_keys(text)
        self.driver.switch_to_default_content()
        self.driver.find_element_by_xpath("//div[@id='b-toolbar__right']/div[3]/div/div[2]/div/div/span").click()

    def is_logged_in(self):
        return self.driver.find_elements_by_id('js-mailbox-exit').count != 0

    def logout_user(self):
        self.driver.find_element_by_id('js-mailbox-exit').click()
