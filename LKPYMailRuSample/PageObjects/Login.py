import time

from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC

class LoginPage(object):
    
    def __init__(self, driver):
        self.driver = driver

    def navigate(self, url):
        self.driver.get(url)

    def login_user(self, login, pwr):
        self.driver.find_element_by_id("mailbox__login").clear()
        self.driver.find_element_by_id("mailbox__login").send_keys(login)
        self.driver.find_element_by_id("mailbox__password").clear()
        self.driver.find_element_by_id("mailbox__password").send_keys(pwr)
        self.driver.find_element_by_id("mailbox__auth__button").click()
        time.sleep(1)